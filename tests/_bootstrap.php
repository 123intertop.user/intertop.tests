<?php

use somethingModules\intertopTests\tests\src\FixtureController;

FixtureController::setFixtureDir(__DIR__ . '/fixtures/template/');

# путь к Yii.php (может быть другим в зависимости от проекта)
require_once __DIR__ . '/../../../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../../../vendor/autoload.php';
