<?php

namespace somethingModules\intertopTests\tests\functional\shop\authorization;

use somethingModules\intertopTests\tests\src\{BaseFunctionalCest, HelperDb};
use somethingModules\intertopTests\tests\FunctionalTester;
use Yandex\Allure\Adapter\Annotation\{
    Features,
    Stories
};

/**
 * @group functional
 *
 * @Features("BaseAuthorization")
 * @Stories("HTTP на авторизацию существующего юзера")
 */
class AuthorizationUserPOSTCest extends BaseFunctionalCest
{
    /**
     * AuthorizationUserPOSTCest constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__);
    }

    /**
     * @param FunctionalTester $I
     * @param \Codeception\Example $data
     *
     * @param HelperDb $helperDb
     * @dataProvider pageProvider
     *
     */
    public function authorizationUserPOST(FunctionalTester $I, \Codeception\Example $data, HelperDb $helperDb)
    {
        $I->prepareActionForTest($data, $this->testHelper);

        //вытягиваем кейс из описания
        $caseNeedCheck = explode(' ', $data['setting']['description'])[0];

        switch ($caseNeedCheck) {
            case 'Case1':
                $I->sendAjaxPostRequest("ua/ajax/common.php", self::encodeQueryParams($data['provider_data']['query_params']));
                $I->seeResponseCodeIs(200);
                $I->seeCookie("authorizationUserCookie", "велью_куки_авторизованного_юзера");
                break;
            case 'Case2':
                // плохая практика апдейтить на лету, необходимо пользоваться фикстурами,
                // и под Case2 нужно просто создать отдельную фикстуру с подобным значением, которое мы ожидаем что зафейлится
                $helperDb->updateInPostgresDB("public.users", ["email" => "faile.intertop.user@gmail.com"]);

                $I->sendAjaxPostRequest("ua/ajax/common.php", self::encodeQueryParams($data['provider_data']['query_params']));
                $I->seeResponseCodeIs(200);
                $I->dontSeeCookie("authorizationUserCookie", "велью_куки_авторизованного_юзера");
                break;
            default:
                throw new \InvalidArgumentException("You dont add this case: $caseNeedCheck");
        }
    }

    protected function encodeQueryParams($query_params)
    {
        # тут должна быть кодировка параметров ровно так как кодируем с под приложения
        return base64_encode($query_params);
    }
}
