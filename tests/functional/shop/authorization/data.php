<?php

return [
    'case1' => [
        'setting' => [
            'description' => 'Case1 Позитивный. HTTP POST, Чек успешного входа существующего юзера',
            'long_description' => '
                    https://jira.local/browse/INTERTOP-12345
                    
                    Отправляем ровно такой же http запрос на авторизацию юзера, который
                    выполняется в форме авторизации/регистрации
					Ожидаем код ответа 200 OK, кука авторизованного юзера успешно проставилась'
        ],
        'fixture_data' => include __DIR__ . '/fixture/case1.php',
        'provider_data' => [
            'query_params' => [
                "LoginForm[userEmail]" => "123intertop.user@gmail.com",
                "LoginForm[password]" => "Qwww123321!",
            ],
            'expected_DB_fields' => [
                'intertop_fixtures' => [
                ],
            ],
        ],
    ],

    'case2' => [
        'setting' => [
            'description' => 'Case2 Негативный. HTTP POST, Чек успешного входа существующего юзера, отправляем парметры НЕсуществующего юзера',
            'long_description' => '
                    https://jira.local/browse/INTERTOP-12345
                    
                    Отправляем ровно такой же http запрос на авторизацию юзера, который
                    выполняется в форме авторизации/регистрации
					Ожидаем код ответа 200 OK, кука авторизованного юзера НЕпроставилась,
					так как мы прапдейтили имя существующего юзера в базе'
        ],
        'fixture_data' => include __DIR__ . '/fixture/case1.php',
        'provider_data' => [
            'query_params' => [
                "LoginForm[userEmail]" => "123intertop.user@gmail.com",
                "LoginForm[password]" => "Qwww123321!",
            ],
            'expected_DB_fields' => [
                'intertop_fixtures' => [
                ],
            ],
        ],
    ],

    //todo https://jira.local/browse/INTERTOP-123 расширить негативными кейсами
];