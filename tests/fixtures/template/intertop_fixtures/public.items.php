<?php

return [
    [
        "id" => 123,
        "name" => "кой-то нейм товара модель 008605071233-773",
        "currency_id" => "UAH",
        "category_id" => "555",
        "price" => 9999,
        "picture" => "[\"https://worldgoods.com.ua/image/cache/catalog/abs123/888/66-600x600.jpg\"]",
        "vendor" => "MARC O`POLO",
        "description" => "какой-то дескрипшн",
        "available" => 1, //есть в наличии
        "url" => "https://intertop.ua/ua/catalog/odezhda/zhenskaya/marc-o-polo-008605071233-773-pd744/",
        "stock_quantity" => 11,
        "created_date" => "02.10.2020 14:34:00",
        "vendors_id" => 111222,
        "picture_upload_status" => 0,
        "article" => "",
    ],
];