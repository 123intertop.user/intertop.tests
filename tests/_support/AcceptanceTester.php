<?php
namespace somethingModules\intertopTests\tests;

use Codeception\{
    Actor,
    Example
};
use somethingModules\intertopTests\tests\_generated\AcceptanceTesterActions;
use somethingModules\intertopTests\tests\src\GlobalHelper;

/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends Actor
{
    use AcceptanceTesterActions;

    /**
     * Метод предварительных штук перед выполнением теста
     *
     * @param Example $data - кейсы из массива data.php
     * @param GlobalHelper $testHelper
     * @param string $startUrl - урл, по которому авторизуемся и стартуем наш тест
     * @throws GuzzleException
     */
    public function prepareActionForTest(
        Example $data,
        GlobalHelper $testHelper,
        string $startUrl
    ): void {
        $I = $this;
        $I->wantTo($data['setting']['description']);
        $testHelper->resetMock();
        $testHelper->clearDB($I, $data);
        $testHelper->loadFixture($I, $data);
        \Yii::$app->redis->flushall();
        $I->amOnPage($startUrl);
    }

    /**
     * метод проходится по provider_data из data.php, асертит ожидаемые поля для чека в БД
     * @param Example $data - кейсы из массива data.php
     * @param bool $checkDontSeeInDB - признак нужен ли асерт на dontSeeInDatabase()
     *
     */
    public function checkAllRecordsInDatabase(Example $data, bool $checkDontSeeInDB = false)
    {
        $I = $this;
        if (isset($data['provider_data']['expected_DB_fields'])) {
            $changedFieldsDb = $data['provider_data']['expected_DB_fields'];
            $I->checkAssertDB($changedFieldsDb, 'expected_DB_fields');
        }

        if ($checkDontSeeInDB) {
            if (isset($data['provider_data']['data_dontSeeInDB'])) {
                $notChangedFieldsDb = $data['provider_data']['data_dontSeeInDB'];
                $I->checkAssertDB($notChangedFieldsDb, 'data_dontSeeInDB');
            }
        }
    }

    /**
     * метод проходится по provider_data[type_check][name_DB] из data.php, асертит ожидаемые поля для чека в БД
     * @param array $assertFieldsDb - массив из data.php для seeInDatabase() или для dontSeeInDatabase()
     * @param string $expected_DB - признак нужен ли асерт на dontSeeInDatabase()
     */
    private function checkAssertDB($assertFieldsDb, string $expected_DB)
    {
        $I = $this;
        foreach ($assertFieldsDb as $dbName => $dbData) {
            $I->amConnectedToDatabase($dbName);
            foreach ($dbData as $tableName => $tableData) {
                foreach ($tableData as $tableRow) {
                    if ($expected_DB == 'expected_DB_fields') {
                        $I->canSeeInDatabase($tableName, $tableRow);
                    } else {
                        $I->cantSeeInDatabase($tableName, $tableRow);
                    }
                }
            }
        }
    }

    /**
     * Define custom actions here
     */
}
