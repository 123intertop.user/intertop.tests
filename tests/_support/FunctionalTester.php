<?php
namespace somethingModules\intertopTests\tests;

use somethingModules\intertopTests\tests\_generated\FunctionalTesterActions;
use somethingModules\intertopTests\tests\src\GlobalHelper;
use Codeception\{
    Actor,
    Example
};


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
 */
class FunctionalTester extends Actor
{
    use FunctionalTesterActions;

    /**
     * метод выполняет подготовительные штуки перед запуском каждого кейса
     *
     * @param Example $data - кейсы из массива data.php
     * @param GlobalHelper $testHelper
     *
     * @throws GuzzleException
     */
    public function prepareActionForTest(
        Example $data,
        GlobalHelper $testHelper
    ) {
        $this->wantTo($data['setting']['description']);
        $testHelper->resetMock();
        $testHelper->clearDB($this, $data);
        $testHelper->loadFixture($this, $data);
        \Yii::$app->redis->flushall();
    }
}
