<?php

namespace somethingModules\intertopTests\tests\acceptance\shop\authorization;

use somethingModules\intertopTests\tests\src\{BaseAcceptanceCest, ConstAutoTest, pages\HeaderPage, pages\MainPage};
use somethingModules\intertopTests\tests\AcceptanceTester;
use somethingModules\intertopTests\tests\src\pages\LoginPage;
use Yandex\Allure\Adapter\Annotation\{
    Features,
    Stories
};

/**
 * @group acceptance
 *
 * @Features("UIBaseAuthorization")
 * @Stories("Вход в акаунт существующего юзера")
 */
class AuthorizationUserCest extends BaseAcceptanceCest
{
    /**
     * AuthorizationCest constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__);
    }

    /**
     * @param AcceptanceTester $I
     * @param \Codeception\Example $data
     *
     * @dataProvider pageProvider
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authorizationUser(AcceptanceTester $I, \Codeception\Example $data)
    {
        $I->prepareActionForTest($data, $this->testHelper, MainPage::route('/ua'));
        HeaderPage::openAuthorizationPopap($I);

        //вытягиваем кейс из описания
        $caseNeedCheck = explode(' ', $data['setting']['description'])[0];

        switch ($caseNeedCheck) {
            case 'Case1':
                $I->waitForElementVisible(LoginPage::AUTH_USER_FORM);
                $I->waitForElementVisible(LoginPage::AUTH_USER_EMAIL_FIELD);
                $I->fillField(LoginPage::AUTH_USER_EMAIL_FIELD, ConstAutoTest::INTERTOP_USER_EMAIL);

                $I->waitForElementVisible(LoginPage::AUTH_USER_PASSWORD_FIELD);
                $I->fillField(LoginPage::AUTH_USER_PASSWORD_FIELD, ConstAutoTest::INTERTOP_USER_PASSWORD);

                $I->waitForElementVisible(LoginPage::ENTRANCE_USER_BUTTON);
                $I->click(LoginPage::ENTRANCE_USER_BUTTON);

                $I->waitForElementNotVisible(LoginPage::AUTH_USER_FORM);

                $I->waitForElementVisible(HeaderPage::AUTH_BUTTON);
                $I->click(HeaderPage::AUTH_BUTTON);

                $I->waitForElementVisible(HeaderPage::LOGGED_USER_MENU);
                $I->seeLink('Мій профіль', '/ua/profile/');
                break;

            case 'Case2':
                $I->waitForElementVisible(LoginPage::AUTH_USER_FORM);
                $I->waitForElementVisible(LoginPage::AUTH_USER_EMAIL_FIELD);
                $I->fillField(LoginPage::AUTH_USER_EMAIL_FIELD, ConstAutoTest::INTERTOP_USER_EMAIL);

                $I->waitForElementVisible(LoginPage::AUTH_USER_PASSWORD_FIELD);
                $I->fillField(LoginPage::AUTH_USER_PASSWORD_FIELD, "password_PASSWORD");

                $I->waitForElementVisible(LoginPage::ENTRANCE_USER_BUTTON);
                $I->click(LoginPage::ENTRANCE_USER_BUTTON);

                $I->waitForElementVisible(LoginPage::INVALID_FEEDBACK);
                $I->assertEquals(
                    "Невірний логін або пароль",
                    $I->grabTextFrom(LoginPage::INVALID_FEEDBACK),
                    "Сравниваем меседж ожидаемый с фактическим"
                );
                //todo доделать чек признака авторизации не залогинило ли нас(вероятно проставляем куку в сессию),
                // чек что кука отсутствует  $I->dontSeeCookie("authorizationUserCookie");
                break;

            default:
                throw new \InvalidArgumentException("You dont add this case: $caseNeedCheck");
        }
    }
}
