<?php

# указываем фикстуры чтобы выполнить предварительный TRUNCATE в базу по указанным таблицам +
# дальнейшего инсерта записей
# у юзера уже есть товар в корзине

return [
    "intertop_fixtures" => [
        'public.users' => [
            [
                "id" => 1,
                "email" => "123autotest.intertop.user@gmail.com",
                "phone" => "+380933332266",
                "agree" => true,
                "created_date" => "02.10.2020 14:34:00",
                "password" => 'ce7a22f5d6eaa29fbe02586865eae03f' //закодирован
            ]
        ],
        'public.users_items' => [
            [
                "id" => 123,
                "user_id" => 1,
                "count_items_in_basket" => 1,
                "purchased_items" => '[{"items_id": 12345, "name": "какой-то нэйм товара"}]',
            ],
        ],
        "public.items" => [
            [
                "id" => 111,
                "name" => "Пальто Marc O’Polo модель 008605071233-773",
                "currency_id" => "UAH",
                "category_id" => "555",
                "price" => 11799,
                "picture" => "[\"https://worldgoods.com.ua/image/cache/catalog/abs123/888/66-600x600.jpg\"]",
                "vendor" => "MARC O`POLO",
                "description" => "какой-то дескрипшн",
                "available" => 1, //есть в наличии
                "url" => "https://intertop.ua/ua/catalog/odezhda/zhenskaya/marc-o-polo-008605071233-773-pd744/",
                "stock_quantity" => 11,
                "created_date" => "02.10.2020 14:34:00",
                "vendors_id" => 111222,
                "picture_upload_status" => 0,
                "article" => "",
            ],
        ],
        "public.categories" => [
            //фикстуры категорий
        ],
        "public.vendors" => [
            //фикстуры производителей
        ],
        //прочие сущности, необходимые для открытия карточки товара
        "public....." => [

        ]
    ],
];