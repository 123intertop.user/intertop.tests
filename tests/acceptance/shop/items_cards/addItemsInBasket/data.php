<?php

return [
    'case1' => [
        'setting' => [
            'description' => 'Case1 Позитивный. 
                              Чек успешного добавления НЕавторизованным юзером товара в корзину, переход на чекаут ',
            'long_description' => '
                    https://jira.local/browse/INTERTOP-12345
                    НО мы загружаем в БД фикстуры из массива "fixture_data"!!!
                    
					Открываем карточку товара НЕавторизованным юзером,
					клик на "Добавить в корзину", открылась корзина,
					чекаем что нейм товара в корзине совпадает с товаром на карточке товара
					клик по "Оформить заказ",
					Ожидаем страницу чекаута с формой входа/регистрации юзера'
        ],
        'fixture_data' => include __DIR__ . '/fixture/case1.php',
        'provider_data' => [
            'expected_DB_fields' => [
                'intertop_fixtures' => [
                    'public.users' => [
                        [
                            "id" => 1,
                            "email" => "autotest.intertop.user@gmail.com",
                            "phone" => "+380931234567",
                            "agree" => true
                        ]
                    ],
                    //чекаем проапдейченные поля count_items_in_basket, purchased_items
                    'public.users_items' => [
                        [
                            "id" => 1,
                            "user_id" => 1,
                            "count_items_in_basket" => 1,
                            "purchased_items" => '[{"items_id": 12345, "name": "какой-то нэйм товара"}]',
                        ],
                    ],
                ],
            ],
            'not_expected_DB_fields' => [
                'intertop_fixtures' => [
                    'public.users' => [
                        [
                            "created_date" => null,
                            "password" => null
                        ],
                    ],
                ]
            ],
        ],
    ],

    'case2' => [
        'setting' => [
            'description' => 'Case2 Позитивный. 
                              Чек успешного добавления Авторизованным юзером товара в корзину, переход на чекаут ',
            'long_description' => '
                    https://jira.local/browse/INTERTOP-12345
                    Предполагается, что у нас голая база => следовательно юзера и моб.телефона в БД нет.
                    НО мы загружаем в БД фикстуры из массива "fixture_data"!!!
                    
					Открываем карточку товара Авторизованным юзером,
					клик на "Добавить в корзину", открылась корзина,
					чекаем что нейм товара в корзине совпадает с товаром на карточке товара
					клик по "Оформить заказ",
					Ожидаем страницу чекаута с формой заполнения полей для Авторизованного юзера'
        ],
        'fixture_data' => include __DIR__ . '/fixture/case2.php',
        'provider_data' => [
            'expected_DB_fields' => [
                'intertop_fixtures' => [
                    //чек, что created_date не изменился
                    'public.users' => [
                        [
                            "id" => 1,
                            "email" => "autotest.intertop.user@gmail.com",
                            "phone" => "+380931234567",
                            "agree" => true,
                            "created_date" => "02.10.2020 14:34:00",
                        ]
                    ],
                    //чекаем проапдейченные поля count_items_in_basket, purchased_items
                    'public.users_items' => [
                        [
                            "id" => 123,
                            "user_id" => 1,
                            "count_items_in_basket" => 2,
                            "purchased_items" =>
                                '[{"items_id": 12345, "name": "какой-то нэйм товара"}, {"items_id": 54321, "name": "какой-то нэйм товара2"}]',
                        ],
                    ],
                ],
            ],
        ],
    ],
];