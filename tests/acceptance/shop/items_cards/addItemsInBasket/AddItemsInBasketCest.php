<?php

namespace somethingModules\intertopTests\tests\acceptance\shop\addItemsInBasket;

use somethingModules\intertopTests\tests\src\{BaseAcceptanceCest, CatalogUrlConst, pages\CardItemsPage, pages\CheckoutPage};
use somethingModules\intertopTests\tests\AcceptanceTester;
use somethingModules\intertopTests\tests\src\pages\LoginPage;
use Yandex\Allure\Adapter\Annotation\{
    Features,
    Stories
};

/**
 * @group acceptance
 *
 * @Features("UIAddItemsInBasket")
 * @Stories("Добавляем товар в корзину, переход на чекаут")
 */
class AddItemsInBasketCest extends BaseAcceptanceCest
{
    private const TEST_ITEM = "/marc-o-polo-008605071233-773-pd744/";
    private const HREF_NAME = '/ua/catalog/odezhda/zhenskaya/marc-o-polo-008605071233-773-pd744/';

    /**
     * AddItemsInBasketCest constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__);
    }

    /**
     * @param AcceptanceTester $I
     * @param \Codeception\Example $data
     *
     * @dataProvider pageProvider
     *
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addItemsInBasket(AcceptanceTester $I, \Codeception\Example $data)
    {
        sleep(3);
        $I->prepareActionForTest($data, $this->testHelper, CatalogUrlConst::ODEZDA_ZHENSKAYA . self::TEST_ITEM);

        $I->waitForElementVisible(CardItemsPage::ADD_TO_BASKET_BUTTON);
        $I->click(CardItemsPage::ADD_TO_BASKET_BUTTON);

        $I->waitForElementVisible(CardItemsPage::BASKET_FORM);
        $I->waitForElementVisible(CardItemsPage::BASKET_ITEMS_NAME);

        $I->seeLink('Пальто Marc O’Polo', self::HREF_NAME);
        $I->waitForElementVisible(CardItemsPage::BY_ITEMS_IN_BASKET_BUTTON);

        //вытягиваем кейс из описания
        $caseNeedCheck = explode(' ', $data['setting']['description'])[0];

        switch ($caseNeedCheck) {
            case 'Case1':
                $I->click(CardItemsPage::BY_ITEMS_IN_BASKET_BUTTON);

                $I->seeCurrentUrlEquals("/ua/checkout/");
                $I->waitForElementVisible(CheckoutPage::H1);
                $I->see('Оформлення замовлення');

                $I->waitForElementVisible(LoginPage::AUTH_USER_FORM);
                break;
            case 'Case2':
                # имитируем залогиненного юзера
//              $I->setCookie("authorizationUserCookie", "велью_куки_авторизованного_юзера");
                $I->click(CardItemsPage::BY_ITEMS_IN_BASKET_BUTTON);

                $I->seeCurrentUrlEquals("/ua/checkout/");
                $I->waitForElementVisible(CheckoutPage::H1);
                $I->see('Оформлення замовлення');

                $I->waitForElementVisible(CheckoutPage::CONTACT_USER_DATA);
                $I->see('Контактні дані');
                break;
            default:
                throw new \InvalidArgumentException("You dont add this case: $caseNeedCheck");
        }

        //todo вынести в хелпер DB,
        // + отдельный функциональный тест на http POST запрос https://jira.local/browse/INTERTOP-789
        $I->canSeeInDatabase(
            "public.users",
            $data["provider_data"]["expected_DB_fields"]["intertop_fixtures"]["public.users"][0]
        );
        $I->canSeeInDatabase(
            "public.users_items",
            $data["provider_data"]["expected_DB_fields"]["intertop_fixtures"]["public.users_items"][0]
        );
    }
}
