<?php

return [
    'case1' => [
        'setting' => [
            'description' => 'Case1 Позитивный, e2e. Авторизация юзера, добавление товара в корзину, переход на чекаут',
            'long_description' => '
                    https://jira.local/browse/INTERTOP-12345
                    Предполагается, что у нас голая база => следовательно юзера и моб.телефона в БД нет.
					Открываем главную, в хедере клик по элементу юзера, открылся попап авторизации,
					клик по "Зарегистрироваться", Вводим инпут поля, отмечаем чекбокс принятия условий,
					клик по "Зарегистрироваться".
					Ожидаем скрытие попапа. Клик в хедере клик по элементу юзера.
					Ожидаем попап залогиненного юзера.'
        ],
        'fixture_data' => include __DIR__ . '/fixture/case1.php',
        'provider_data' => [
            'test_item' => '/marc-o-polo-008605071233-773-pd744/',
            'items_name' => '/Пальто Marc O’Polo',
            'href_name' => '/ua/catalog/odezhda/zhenskaya/marc-o-polo-008605071233-773-pd744/',
            'expected_DB_fields' => [
                'intertop_fixtures' => [
                    'public.users' => [
                        [
                            "id" => 1,
                            "email" => "autotest.intertop.user@gmail.com",
                            "phone" => "+380931234567",
                            "agree" => true
                        ]
                    ],
                    //чекаем проапдейченные поля count_items_in_basket, purchased_items
                    'public.users_items' => [
                        [
                            "id" => 1,
                            "user_id" => 1,
                            "count_items_in_basket" => 1,
                            "purchased_items" => '[{"items_id": 12345, "name": "какой-то нэйм товара"}]',
                        ],
                    ],
                ],
            ],
            'not_expected_DB_fields' => [
                'intertop_fixtures' => [
                    'public.users' => [
                        [
                            "created_date" => null,
                            "password" => null
                        ],
                    ],
                ]
            ],
        ],
    ],
];
