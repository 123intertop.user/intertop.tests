<?php

# указываем фикстуры чтобы выполнить предварительный TRUNCATE в базу по указанным таблицам +
# дальнейшего инсерта записей
# у юзера еще нет товаров в корзине, public.users_items пустая

return [
    "intertop_fixtures" => [
        'public.users' => [
            [
                "id" => 1,
                "email" => "123autotest.intertop.user@gmail.com",
                "phone" => "+380933332266",
                "agree" => true,
                "created_date" => "02.10.2020 14:34:00",
                "password" => 'ce7a22f5d6eaa29fbe02586865eae03f' //закодирован
            ],
        ],
        'public.users_items' => [
            [
            ],
        ],
    ],
];