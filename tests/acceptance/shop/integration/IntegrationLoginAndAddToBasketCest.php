<?php

namespace somethingModules\intertopTests\tests\acceptance\shop\integration;

use somethingModules\intertopTests\tests\src\{BaseAcceptanceCest,
    pages\CardItemsPage,
    pages\MainPage};
use Codeception\Actor;
use somethingModules\intertopTests\tests\AcceptanceTester;
use somethingModules\intertopTests\tests\src\pages\LoginPage;
use Yandex\Allure\Adapter\Annotation\{
    Features,
    Stories
};

/**
 * @group acceptance
 * @group e2e
 *
 * @Features("UIBaseIntegration")
 * @Stories("логин и переход на чекаут")
 */
class IntegrationLoginAndAddToBasketCest extends BaseAcceptanceCest
{
    /**
     * IntegrationLoginAndAddToBasketCest constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__);
    }

    /**
     * @param AcceptanceTester $I
     * @param \Codeception\Example $data
     *
     * @dataProvider pageProvider
     *
     * @throws \Exception
     */
    public function integrationLoginAndAddToBasket(AcceptanceTester $I, \Codeception\Example $data)
    {
        $provider_data = $data["provider_data"];

        $I->prepareActionForTest($data, $this->testHelper, MainPage::route('/ua'));
        LoginPage::authorizationUser($I);
        CardItemsPage::addItemsInBasketFromCardItems(
            $I,
            $provider_data['test_item'],
            $provider_data['items_name'],
            $provider_data['href_name']
        );

        //todo вынести в хелпер DB, а лучше отдельный функциональный тест на http POST запрос
        // https://jira.local/browse/INTERTOP-8910
        $I->canSeeInDatabase(
            "public.users_items",
            $provider_data["expected_DB_fields"]["intertop_fixtures"]["public.users"][0]
        );
    }
}

//todo добавить переход на каталог и подтверждение заказа на чекауте  https://jira.local/browse/INTERTOP-5678
