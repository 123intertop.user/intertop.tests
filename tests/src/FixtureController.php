<?php

namespace somethingModules\intertopTests\tests\src;

/**
 * Class FixtureController
 *
 * Данный класс, предназначен для загрузки, в статическое хранилище,
 * всех щаблонов которые размещены в FIXTURES_DIR
 *
 * Для загрузки фикстуры, вам сначала нужно создать шаблон фикстуры
 *
 * Переменная $fixtureDir определяется в tests/_bootstrap.php
 *
 */
class FixtureController
{
    protected static $fixtures = [];
    protected static $fixturesDir;


    public static function loadFixtureFromDir()
    {
        $fixtureDir = self::getFixtureDir();
        self::cleanup();
        if (file_exists($fixtureDir)) {
            $allDB = array_diff(scandir($fixtureDir), array('..', '.'));
            foreach ($allDB as $db) {
                $files = scandir($fixtureDir . $db);
                foreach ($files as $fileName) {
                    $templateFixture = $fixtureDir . $db . "/" . $fileName;
                    if (is_file($templateFixture)) {
                        $tableName = str_replace('.php', '', $fileName);
                        self::add($db, $tableName, require($templateFixture));
                    }
                }
            }
        }
    }

    public static function setFixtureDir($pathToFixtureDir)
    {
        self::$fixturesDir = $pathToFixtureDir;
    }

    public static function add($dbName, $name, $data)
    {
        self::$fixtures[$dbName][$name] = $data;
    }

    public static function getFixtureDir()
    {
        return self::$fixturesDir;
    }

    public static function get($dbName, $name)
    {
        if (!self::exists($dbName, $name)) {
            throw new \RuntimeException("$name not found in fixtures");
        }

        return self::$fixtures[$dbName][$name];
    }

    public static function exists($dbName, $name)
    {
        return isset(self::$fixtures[$dbName][$name]);
    }

    public static function cleanup()
    {
        self::$fixtures = [];
    }

    public static function getAllDB()
    {
        return array_keys(self::$fixtures);
    }
}
