<?php

namespace somethingModules\intertopTests\tests\src;

/**
 * Class BaseFunctionalCest
 *
 */
class BaseFunctionalCest
{
    /**
     * @var GlobalHelper $testHelper
     */
    protected $testHelper;

    /**
     * GetMainCest constructor.
     * Конструктор класса GetMainCest
     *
     * TestHelper через него идет создание фикстур моков, обработка файла data.php,
     *
     * @param string $cestDirectory каталог (__DIR__) класса, который является потомком данного базового класса
     */
    public function __construct(string $cestDirectory = __DIR__)
    {
        $this->testHelper = new GlobalHelper($cestDirectory);
    }

    /**
     * GetMainCest dataProvider
     * Дата провайдер теста GetMainCest
     *
     * В данной функции реализуется дата провайдер который возвращает все кейсы с data.php
     * Далее функция getMainTest обрабатывает каждый кейс
     *
     * @return array
     */
    protected function pageProvider()
    {
        return $this->testHelper->getDataProvider();
    }
}
