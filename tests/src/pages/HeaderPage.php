<?php

namespace somethingModules\intertopTests\tests\src\pages;

use Codeception\Actor;
use Exception;
use somethingModules\intertopTests\tests\AcceptanceTester;

class HeaderPage extends BasePage
{
    protected static $URL = '';


    public const AUTH_BUTTON = "//div[@id='auth_block']";
    public const LOGGED_USER_MENU = "//*[@class='bl-reg-log-choose']";


    /**
     * @param AcceptanceTester|Actor $I
     * @throws Exception
     */
    public static function openAuthorizationPopap(Actor $I)
    {
        $I->waitForElementVisible(self::AUTH_BUTTON);
        $I->click(self::AUTH_BUTTON);
    }

}
