<?php

namespace somethingModules\intertopTests\tests\src\pages;

class CheckoutPage extends BasePage
{
    public const H1  = "//*[@class='heading flex-center-v']/h1";
    public const CONTACT_USER_DATA  = "//*[@class='fields-in cart-section-in']/h2";
}
