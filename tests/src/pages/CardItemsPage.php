<?php

namespace somethingModules\intertopTests\tests\src\pages;

use Codeception\Actor;
use somethingModules\intertopTests\tests\AcceptanceTester;
use somethingModules\intertopTests\tests\src\CatalogUrlConst;

class CardItemsPage extends BasePage
{
    public const ADD_TO_BASKET_BUTTON = "//span[@id='basket_add_preview']";
    public const BASKET_FORM = "//div[@class='intertop-basket-popup']";
    public const BASKET_ITEMS_NAME = "//div[@class='basket-item__name']/div/a";

    public const BY_ITEMS_IN_BASKET_BUTTON = "//div[@class='button styl-material']/a";


    /**
     *
     * @param AcceptanceTester|Actor $I
     * @throws \Exception
     * @param string $test_item
     * @param string $items_name
     * @param string $href_name
     */
    public static function addItemsInBasketFromCardItems($I, string $test_item, string $items_name, string $href_name)
    {
        $I->amOnPage(CatalogUrlConst::ODEZDA_ZHENSKAYA . $test_item);

        $I->waitForElementVisible(self::ADD_TO_BASKET_BUTTON);
        $I->click(self::ADD_TO_BASKET_BUTTON);

        $I->waitForElementVisible(self::BASKET_FORM);
        $I->waitForElementVisible(self::BASKET_ITEMS_NAME);

        $I->seeLink($items_name, $href_name);

        $I->waitForElementVisible(self::BY_ITEMS_IN_BASKET_BUTTON);
        $I->click(self::BY_ITEMS_IN_BASKET_BUTTON);

        $I->seeCurrentUrlEquals("/ua/checkout/");
        $I->waitForElementVisible(CheckoutPage::H1);
        $I->see('Оформлення замовлення');
    }
}
