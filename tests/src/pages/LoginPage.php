<?php

namespace somethingModules\intertopTests\tests\src\pages;

use Codeception\Actor;
use somethingModules\intertopTests\tests\src\ConstAutoTest;
use somethingModules\intertopTests\tests\AcceptanceTester;

class LoginPage extends BasePage
{
    protected static $URL = '';


    public const AUTH_USER_FORM = "//div[@class='auth-page']";
    public const REGISTRATION_FORM = "//*[@class='user-registration log-fields act']";

    public const ENTRANCE_USER_BUTTON  = "//div[contains(@class,'login-action')]";
    public const REGISTRATION_BUTTON = "//*[contains(@class,'register-action')]";

    public const AUTH_USER_EMAIL_FIELD = "//input[@id='email']";
    public const AUTH_USER_PHONE_FIELD = "//input[@id='phone']";
    public const AUTH_USER_PASSWORD_FIELD = "//input[@id='password']";

    public const INVALID_FEEDBACK = "//*[@class='invalid-feedback d-block']";


    /**
     *
     * @param AcceptanceTester|Actor $I
     * @throws \Exception
     */
    public static function authorizationUser(Actor $I)
    {
//        $I->amOnPage(self::route('/ua'));
        HeaderPage::openAuthorizationPopap($I);
        $I->waitForElementVisible(self::AUTH_USER_FORM);
        $I->waitForElementVisible(self::AUTH_USER_EMAIL_FIELD);
        $I->fillField(self::AUTH_USER_EMAIL_FIELD, ConstAutoTest::INTERTOP_USER_EMAIL);

        $I->waitForElementVisible(self::AUTH_USER_PASSWORD_FIELD);
        $I->fillField(self::AUTH_USER_PASSWORD_FIELD, ConstAutoTest::INTERTOP_USER_PASSWORD);

        $I->waitForElementVisible(self::ENTRANCE_USER_BUTTON);
        $I->click(self::ENTRANCE_USER_BUTTON);

        $I->waitForElementNotVisible(self::AUTH_USER_FORM);

        $I->waitForElementVisible(HeaderPage::AUTH_BUTTON);
        $I->click(HeaderPage::AUTH_BUTTON);

        $I->waitForElementVisible(HeaderPage::LOGGED_USER_MENU);
        $I->seeLink('Мій профіль', '/ua/profile/');
#        $I->seeCookie("authorizationUserCookie", "велью_куки_авторизованного_юзера");
    }
}
