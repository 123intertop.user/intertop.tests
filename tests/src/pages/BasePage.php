<?php

namespace somethingModules\intertopTests\tests\src\pages;

use Codeception\Actor;

class BasePage extends Actor
{

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     * @param $param
     * @return string
     */
    public static function route($param)
    {
        return static::$URL . $param;
    }
}
