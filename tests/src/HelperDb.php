<?php

namespace somethingModules\intertopTests\tests\src;

use somethingModules\intertopTests\tests\_generated\FunctionalTesterActions;

class HelperDb
{
    use FunctionalTesterActions;


    /**
     * @param string $table
     * @param array $data
     */
    public function updateInPostgresDB(string $table, array $data)
    {
        $this->amConnectedToDatabase('intertop_fixtures');
        $this->updateInDatabase($table, $data);
    }

    /**
     * @param string $table
     * @param array $data
     */
    public function updateInMysqlDB(string $table, array $data)
    {
        $this->amConnectedToDatabase('mysql_intertop_fixtures');
        $this->updateInDatabase($table, $data);
    }

    //todo https://jira.local/browse/INTERTOP-345678 расширить возможность хелпера, инсерт, транкейт,
    // ифы в зависимости от базы mysql/pgsql и т.д.
}
