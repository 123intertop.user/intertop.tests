<?php


namespace somethingModules\intertopTests\tests\src;

class GlobalHelper
{
    /**
     * @var mixed - весь массив с data.php
     */
    private $data;

    /**
     * GlobalHelper constructor.
     * @param $patchToDir - путь к папке теста
     */
    public function __construct($patchToDir)
    {
        $patchToData = $patchToDir . '/data.php';

        $this->data = include $patchToData;

        FixtureController::loadFixtureFromDir();
    }

    /**
     *  Получение массива для дата провайдера
     * @param string $case - ручная выборка кейсов (значения добавлять через пробел)
     * @return mixed
     */
    public function getDataProvider($case = "")
    {
        $allData = $this->data;
        if ($case == "") {
            return $allData;
        } else {
            $casesNeed = explode(" ", $case);
            $result = array();
            foreach ($casesNeed as $caseNeed) {
                if (array_key_exists($caseNeed, $allData)) {
                    $result[$caseNeed] = $allData[$caseNeed];
                }
            }
            return $result;
        }
    }

    /**
     * Метод для загрузки фикстур
     * Данные метод берет вашу фикстуру обновляет заготовленный шаблон фикстуры в папке app/test/fixture
     * И загружает фикстуры в базу
     *
     * После прохождения теста фикстуры автоматически очищаются
     *
     * @param $I - нужно передавать с теста, для того что бы haveInDatabase() знал куда писать фикстуры
     * @param $case - данные с dataProvider
     * @param bool $disableTemplate - Не обязательный параметр, позволяет отключить шаблоны
     */
    public function loadFixture($I, $case, $disableTemplate = false)
    {
        if (isset($case['fixture_data']) && !empty($case['fixture_data'])) {
            $allDB = $this->getDbFromFixture($case['fixture_data'], $disableTemplate);
            foreach ($allDB as $dbName) {
                $I->amConnectedToDatabase($dbName);
                foreach ($case['fixture_data'][$dbName] as $keyTable => $dataTable) {
                    foreach ($dataTable as $fixtureUpdateData) {
                        $resultFixture = $this->updateFixture($dbName, $keyTable, $fixtureUpdateData, $disableTemplate);
                        $I->haveInDatabase($keyTable, $resultFixture);
                    }
                }
            }
        }
    }

    /**
     * Системный метод для сравнения, есть ли базы c data.php в FixtureTemplate
     *
     * @param $fixtureData - массив фикстур с data.php
     * @param bool $disableTemplate - Параметр который означает нужно ли использовать шаблоны фикстур true/false
     * @return array - возвращает массив баз
     */
    private function getDbFromFixture($fixtureData, $disableTemplate = false)
    {
        $fixtureDbArray = array_keys($fixtureData);
        if (!$disableTemplate) {
            $templateDB = FixtureController::getAllDB();
            foreach ($fixtureDbArray as $fixtureDb) {
                if (array_search($fixtureDb, $templateDB) === false) {
                    throw new \RuntimeException("DB $fixtureDb not found in template fixture");
                }
            }
            return $fixtureDbArray;
        } else {
            return $fixtureDbArray;
        }
    }

    /**
     * Системный метод для обновление данных в шаблоне фикстур
     *
     * @param $dbName - Название базы
     * @param $fixtureName - Название фиктуры
     * @param $dataForUpdate - Данные для обновления
     * @param bool $disableTemplate - Не обязательный параметр, позволяет отключить шаблоны
     * @return array - Возвращает готовую фикстуру
     */
    private function updateFixture($dbName, $fixtureName, $dataForUpdate, $disableTemplate = false)
    {
        if (!$disableTemplate) {
            $fixtureTemplate = FixtureController::get($dbName, $fixtureName);
            $result = array_merge($fixtureTemplate[0], $dataForUpdate);
        } else {
            $result = $dataForUpdate;
        }
        return $result;
    }

    /**
     * Метод для очистки таблиц в определенной базе
     *
     * Данные метод берет в фикстуре определенную базу
     * И очищает таблицы которые в ней записаны
     *
     *
     * @param $I
     * @param $case - данные с dataProvider
     * @param $dbName - название базы
     *
     */
    public function clearDB($I, $case, $dbName)
    {
        //todo clearDB()
    }

    /**
     *
     * Обнуление всех моков в мок-сервере(заглушки для внешних сервисов)
     *
     */
    public static function resetMock(){
        //todo resetMock()
    }
    
}